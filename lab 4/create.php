<?php
    if (isset($_POST['submit'])) {

        $xml = new DOMDocument();
        $xml->preserveWhiteSpace = false;
        $xml->formatOutput = true;
        $xml->load('data/data.xml');

        $newName = $_POST["name"];
        $newPrice = $_POST["price"];
        $newDescr = $_POST["descr"];
        echo $newId = $xml->getElementsByTagName("products")->item(0)->getElementsByTagName("product")->length + 1;

        $products = $xml->documentElement;

        $newProduct = $xml->createElement("product");

        $id = $xml->createElement('id', $newId);
        $newProduct->appendChild($id);

        $name = $xml->createElement('name', $newName);
        $newProduct->appendChild($name);

        $price = $xml->createElement('price', $newPrice);
        $newProduct->appendChild($price);

        $descr = $xml->createElement('descr', $newDescr);
        $newProduct->appendChild($descr);

        $products->appendChild($newProduct);
        
        $xml->save("data/data.xml");

        header('location: index.php?page = list');
    }
    ?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>

        <form class="create-form" method="post" enctype="multipart/form-data">
            <input type="text" name="name" placeholder="Называние" required>
            <input type="number" name="price" placeholder="Стоимость" required>
            <textarea name="descr" placeholder="Описание" required></textarea>
            <input type="submit" name="submit" value="Добавить">
        </form>
    </body>
</html>
