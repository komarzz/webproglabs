<?php

    $xml = new DOMDocument();
    $xml->preserveWhiteSpace = false;
    $xml->formatOutput = true;
    $xml->load('data/data.xml') or die("death");
    $id = $_GET['id'];
    $products = $xml->getElementsByTagName("product");

    foreach ($products as $product) {
        $productId = $product->getElementsByTagName("id")->item(0)->nodeValue;
        if($productId == $id){
            echo $product->getElementsByTagName("id")->item(0)->nodeValue;
            $xml->getElementsByTagName("products")->item(0)->removeChild($product);
            break;
        }
    }

    $xml->save("data/data.xml");
    header('location: index.php?page = list');
?>
