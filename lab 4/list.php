<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <?php
            $xml = simplexml_load_file("data/data.xml") or die("Error: Cannot create object");
        ?>

        <div class="wrapper">
            <a href="index.php?page=create"><button type="button" class="add">Добавить</button></a>
            <div class="items">
                <?php foreach ($xml->product as $value): ?>
                    <div class="item">
                        <h1><?php echo $value->name ?></h1>
                        <h2><?php echo $value->price ?></h2>
                        <p><?php echo $value->descr ?></p>

                        <a href="index.php?page=update&id=<?php echo $value->id ?>">
                            <button type="button" >
                                Редактировать
                            </button>
                        </a>

                        <a href="index.php?page=delete&id=<?php echo $value->id ?>">
                            <button onclick="return confirmation('<?php echo $value->name ?>')">
                                Удалить
                            </button>
                        </a>

                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </body>
</html>

<script>
    function confirmation(name){
        return confirm("Вы правда хотите удалить букет \""+name+"\"?");
    }
</script>
