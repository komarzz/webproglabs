<?php
    $xml = new DOMDocument();
    $xml->preserveWhiteSpace = false;
    $xml->formatOutput = true;
    $xml->load('data/data.xml') or die("death");
    $id = $_GET['id'];
    $products = $xml->getElementsByTagName("product");

    foreach ($products as $product) {
        $productId = $product->getElementsByTagName("id")->item(0)->nodeValue;
        if($productId == $id){
            $oldProduct = $product;
            $oldName = $product->getElementsByTagName("name")->item(0)->nodeValue;
            $oldPrice = $product->getElementsByTagName("price")->item(0)->nodeValue;
            $oldDescr = $product->getElementsByTagName("descr")->item(0)->nodeValue;
            break;
        }
    }

    if (isset($_POST['submit'])) {

        $newName = $_POST["name"];
        $newPrice = $_POST["price"];
        $newDescr = $_POST["descr"];

        $products = $xml->documentElement;

        $newProduct = $xml->createElement("product");

        $id = $xml->createElement('id', $id);
        $newProduct->appendChild($id);

        $name = $xml->createElement('name', $newName);
        $newProduct->appendChild($name);

        $price = $xml->createElement('price', $newPrice);
        $newProduct->appendChild($price);

        $descr = $xml->createElement('descr', $newDescr);
        $newProduct->appendChild($descr);

        $products->replaceChild($newProduct, $oldProduct);

        $xml->save("data/data.xml");

        header('location: index.php?page = list');
    }
 ?>

 <!DOCTYPE html>
 <html lang="en" dir="ltr">
     <head>
         <meta charset="utf-8">
         <title></title>
     </head>
     <body>
         <form class="create-form" method="post" enctype="multipart/form-data">
             <input type="text" value="<?php echo $oldName ?>" name="name" placeholder="Называние" required>
             <input type="number" value="<?php echo $oldPrice ?>" name="price" placeholder="Стоимость" required>
             <textarea name="descr" placeholder="Описание" required><?php echo $oldDescr ?></textarea>
             <input type="submit" name="submit" value="Обновить">
         </form>
     </body>
 </html>
