<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <?php
            $serverName = "localhost";
            $userName = "root";
            $password = "";
            $database = "McDonalds";

            $connection = new mysqli($serverName, $userName, $password, $database);

            if ($connection->connect_error) {
                die ("Connection failed: " . $connection->connect_error);
            }

            $sql = "SELECT * FROM menu";
            $result = $connection->query($sql);

            if (!$result){
                die("Invalid query: " . $connection->error);
            }
        ?>

        <div class="wrapper">
            <a href="index.php?page=create"><button type="button" class="add">Добавить</button></a>
            <div class="items">

                <?php while ($row = $result->fetch_assoc()): ?>

                        <div class="item">
                            <h1><?php echo $row["name"]?></h1>
                            <h2><?php echo $row["price"]?></h2>
                            <p><?php echo $row["descr"]?></p>

                            <a href="index.php?page=update&id=<?php echo $row["id"] ?>">
                                <button type="button" >
                                    Редактировать
                                </button>
                            </a>

                            <a href="index.php?page=delete&id=<?php echo $row["id"] ?>">
                                <button onclick="return confirmation('<?php echo $row["name"] ?>')">
                                    Удалить
                                </button>
                            </a>
                        </div>



                <?php endwhile; ?>

        </div>
    </body>
</html>

<script>
    function confirmation(name){
        return confirm("Вы правда хотите удалить позицию \""+name+"\"?");
    }
</script>
