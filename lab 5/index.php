<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Lab4</title>
    </head>
        <body>
            <?php
                if (isset($_GET['page'])) {
                    switch ($_GET['page']) {
                        case 'list':
                            require_once 'list.php';
                            break;
                        case 'create':
                            require_once 'create.php';
                            break;
                        case 'update':
                            require_once 'update.php';
                            break;
                        case 'delete':
                            require_once 'delete.php';
                            break;
                    }
                } else {
                    require_once 'list.php';
                }
            ?>
        </body>
</html>
