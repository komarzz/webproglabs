"use strict"


function createTable() {
    document.getElementById("createTableButton").disabled = true;
    document.getElementById("createRowButton").disabled = false;
    document.getElementById("deleteRowButton").disabled = false;
    var newTable = document.createElement('table');
    newTable.setAttribute('id', 'table');
    newTable.setAttribute('border', '1');
    document.getElementById('table-spot').appendChild(newTable);
}

function createRow() {
    var table = document.getElementById('table');
    var newRow = document.createElement('tr');
    createCells(newRow, table.rows.length);
    table.appendChild(newRow);
}

function createCells(row, rowsCount) {
    let newCell;
    let newTextInCell;
    for (let i = 0; i <= 3; i++) {
        newCell = document.createElement('td');
        newTextInCell = document.createTextNode(rowsCount + '.' + i);
        newCell.appendChild(newTextInCell);
        row.appendChild(newCell);
    }
}

function deleteRow() {
    var rowNumberInput = document.getElementById('input');
    if (checkDeleteRowInputValidity(rowNumberInput)) {
        var rowNumber = Number(rowNumberInput.value);
        var table = document.getElementById("table");
        table.deleteRow(rowNumber);
    }
    else {
        alert("Wrong Input")
    }
}

function checkDeleteRowInputValidity(input) {
    var table = document.getElementById('table');
    if (Number(input.value) >= table.rows.length || Number(input.value) < 0 || !(/^[0-9]+$/.test(input.value))) {
        return false;
    } else {
        return true;
    }
}

function alertInput() {
    var userInput = document.getElementById('input').value;
    alert(userInput);
}
